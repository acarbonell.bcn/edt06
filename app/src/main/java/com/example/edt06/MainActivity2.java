package com.example.edt06;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;

public class MainActivity2 extends AppCompatActivity {
    public static String IMG_TITLE = "com.example.edt06.IMG_TITLE";

    private ImageView image1;
    private ImageView image2;
    private ImageView image3;
    private ImageView image4;
    private ImageView sStop;
    private ImageView sGo;

    ObjectAnimator oA1;
    ObjectAnimator oA2;
    ObjectAnimator oA3;
    ObjectAnimator oA4;
    ObjectAnimator oA5;
    ObjectAnimator oA6;
    ObjectAnimator oA7;
    ObjectAnimator oA8;
    ObjectAnimator oA9;
    ObjectAnimator oA10;
    ObjectAnimator oA11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        image1 = findViewById(R.id.img21);
        image2 = findViewById(R.id.img22);
        image3 = findViewById(R.id.img23);
        image4 = findViewById(R.id.img24);
        sStop = findViewById(R.id.sStop);
        sGo = findViewById(R.id.sGo);


        oA1 = ObjectAnimator.ofFloat(image1, "rotationY", 0f, 180f);
        oA1.setDuration(500);
        oA2 = ObjectAnimator.ofFloat(image1, "translationX", 0f, -500f);
        oA2.setStartDelay(500);
        oA3 = ObjectAnimator.ofFloat(image2, "rotationX", 0f, 720f);
        oA3.setDuration(700);
        oA4 = ObjectAnimator.ofFloat(image2, "translationX", 0f, 500f);
        oA4.setStartDelay(700);

        oA5 = ObjectAnimator.ofFloat(image3, "alpha", 00, 1);
        oA5.setDuration(500);

        oA7 = ObjectAnimator.ofFloat(sStop, "alpha", 0, 1);
        oA7.setDuration(500);
        oA8 = ObjectAnimator.ofFloat(sStop, "alpha", 1, 0);
        oA8.setDuration(500);
        oA8.setStartDelay(500);
        oA9 = ObjectAnimator.ofFloat(sGo, "alpha", 0, 1);
        oA9.setDuration(500);
        oA9.setStartDelay(1000);
        oA10 = ObjectAnimator.ofFloat(sGo, "alpha", 1, 0);
        oA10.setDuration(500);
        oA10.setStartDelay(1500);
        oA11 = ObjectAnimator.ofFloat(image4, "translationX", 0f, 500f);
        oA11.setDuration(500);
        oA11.setStartDelay(1500);


        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int imgInfo = R.drawable.img2;

                Intent intent = new Intent(MainActivity2.this, MainActivity3.class);
                intent.putExtra(IMG_TITLE, imgInfo);
                AnimatorSet animationSet = new AnimatorSet();
                animationSet.playTogether(oA1, oA2);
                animationSet.start();
                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                                MainActivity2.this
                                //Pair.create(image1,"imgTrans")
                        );
                        startActivity(intent, options.toBundle());
                    }
                });
            }
        });


        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int imgInfo = R.drawable.img3;

                Intent intent = new Intent(MainActivity2.this, MainActivity3.class);
                intent.putExtra(IMG_TITLE, imgInfo);
                AnimatorSet animationSet = new AnimatorSet();
                animationSet.playTogether(oA3, oA4);
                animationSet.start();
                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                                MainActivity2.this
                                //Pair.create(image3,"imgTrans")
                        );
                        startActivity(intent, options.toBundle());
                    }
                });
            }
        });
        image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int imgInfo = R.drawable.img4;

                Intent intent = new Intent(MainActivity2.this, MainActivity3.class);
                intent.putExtra(IMG_TITLE, imgInfo);
                AnimatorSet animationSet = new AnimatorSet();
                animationSet.playTogether(oA5);
                animationSet.start();
                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                                MainActivity2.this
                                //Pair.create(image3,"imgTrans")
                        );
                        startActivity(intent, options.toBundle());
                    }
                });
            }
        });

        image4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int imgInfo = R.drawable.img5;

                Intent intent = new Intent(MainActivity2.this, MainActivity3.class);
                intent.putExtra(IMG_TITLE, imgInfo);
                AnimatorSet animationSet = new AnimatorSet();
                animationSet.playTogether(oA7, oA8, oA9, oA10, oA11);
                animationSet.start();
                animationSet.start();
                animationSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                                MainActivity2.this
                                //Pair.create(image3,"imgTrans")
                        );
                        startActivity(intent, options.toBundle());
                    }
                });
            }
        });





    }
}