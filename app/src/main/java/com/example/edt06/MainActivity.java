package com.example.edt06;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    //private static final long SPLASH_SCREEN = 2000;
    private ImageView imgOrigen1;
    private ImageView imgOrigen2;
    private ImageView bgOrigen;
    private TextView textOrigen;
    private androidx.gridlayout.widget.GridLayout glO1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgOrigen1 = findViewById(R.id.imgO1);
        imgOrigen2 = findViewById(R.id.imgO2);
        bgOrigen = findViewById(R.id.bgOrigen);
        textOrigen = findViewById(R.id.textTrans1);
        glO1 = findViewById(R.id.glO1);





        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                //Works only fro API > 21, then we need to surronded by this if as suggested
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                            MainActivity.this,
                            Pair.create(imgOrigen1,"imgTrans1"),
                            Pair.create(imgOrigen2,"imgTrans2"),
                            Pair.create(textOrigen, "textTrans"),
                            Pair.create(bgOrigen, "bgTrans"),
                            Pair.create(glO1, "glTrans")

                    );

                    //objectAnimator = ObjectAnimator.ofFloat(bgOrigen, "translationX", 0f, 180f);
                    startActivity(intent,options.toBundle());
                }
            }
        }, 2000);


    }
}