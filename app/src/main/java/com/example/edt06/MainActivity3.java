package com.example.edt06;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity3 extends AppCompatActivity {
    private ImageView imgInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        imgInfo = findViewById(R.id.imgCenter);

        Intent i = getIntent();

        int iCenter = i.getIntExtra(MainActivity2.IMG_TITLE, 0);
        imgInfo.setImageResource(iCenter);
    }
}